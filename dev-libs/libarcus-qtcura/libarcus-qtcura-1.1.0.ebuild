EAPI=8

PYTHON_COMPAT=( python3_{10..13} )
inherit cmake python-single-r1 git-r3

MY_PN="libArcus"

DESCRIPTION="This library facilitates communication between Cura and its backend"
HOMEPAGE="https://gitlab.com/qtcura/libarcus"
EGIT_REPO_URI="https://gitlab.com/qtcura/libarcus.git"
EGIT_BRANCH="master"

KEYWORDS="~amd64 ~x86"

LICENSE="LGPL-3+"
SLOT="0/3"
IUSE="examples python static-libs"
KEYWORDS="~amd64 ~x86"

RDEPEND="${PYTHON_DEPS}
	dev-libs/protobuf:=
	$(python_gen_cond_dep '
        dev-python/sip[${PYTHON_USEDEP}]
        python? ( dev-python/protobuf-python[${PYTHON_USEDEP}] )
    ')"
DEPEND="${RDEPEND}"
REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

pkg_setup() {
	use python && python-single-r1_pkg_setup
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_PYTHON=$(usex python ON OFF)
		-DBUILD_EXAMPLES=$(usex examples ON OFF)
		-DBUILD_STATIC=$(usex static-libs ON OFF)
	)
	use python && mycmakeargs+=( -DPYTHON_VERSION=`python -c 'import sys; version=sys.version_info[:3]; print("{0}.{1}.{2}".format(*version))'` )
	cmake_src_configure
}
