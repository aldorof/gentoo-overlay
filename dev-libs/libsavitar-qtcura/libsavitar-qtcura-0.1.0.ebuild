EAPI=7

PYTHON_COMPAT=( python3_{6..8} )
inherit cmake-utils python-single-r1 git-r3

DESCRIPTION="libSavitar is a c++ implementation of 3mf loading with SIP python bindings"
HOMEPAGE="https://gitlab.com/qtcura/libsavitar"
EGIT_REPO_URI="https://gitlab.com/qtcura/libsavitar.git"
EGIT_BRANCH="master"

KEYWORDS="~amd64 ~x86"

LICENSE="AGPL-3+"
SLOT="0/2"
IUSE="python static-libs"
KEYWORDS="~amd64 ~x86"

RDEPEND="${PYTHON_DEPS}
	$(python_gen_cond_dep '
		dev-python/sip[${PYTHON_MULTI_USEDEP}]
		>=dev-python/protobuf-python-3:*[${PYTHON_MULTI_USEDEP}]
	')
	dev-libs/pugixml"
DEPEND="${RDEPEND}"
REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

pkg_setup() {
	use python && python-single-r1_pkg_setup
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_PYTHON=$(usex python ON OFF)
		-DBUILD_STATIC=$(usex static-libs ON OFF)
		-DBUILD_PUGIXML=OFF
	)
	use python && mycmakeargs+=( -DPYTHON_SITE_PACKAGES_DIR="$(python_get_sitedir)" )
	cmake-utils_src_configure
}
