EAPI=8

inherit cmake git-r3

DESCRIPTION="C++ part of Clipper library by Angus Johnson."
HOMEPAGE="https://gitlab.com/qtcura/polyclipping"
EGIT_REPO_URI="https://gitlab.com/qtcura/polyclipping.git"
EGIT_BRANCH="master"
EGIT_COMMIT="v6.4.2"

KEYWORDS="~amd64 ~x86"

LICENSE="MIT"
SLOT="0"
IUSE="static-libs"

RDEPEND=""
DEPEND="${RDEPEND}"
REQUIRED_USE=""

src_configure() {
	local mycmakeargs=(
		-DBUILD_STATIC=$(usex static-libs ON OFF)
	)
	cmake_src_configure
}
