EAPI="8"

inherit cmake git-r3

DESCRIPTION="A 3D model slicing engine for 3D printing"
HOMEPAGE="https://gitlab.com/qtcura/qtcuraengine"
EGIT_REPO_URI="https://gitlab.com/qtcura/qtcuraengine.git"
EGIT_BRANCH="master"
KEYWORDS="~amd64 ~x86"

LICENSE="AGPL-3"
SLOT="0"
IUSE="+arcus doc openmp test"

RDEPEND="
	arcus? (
		dev-libs/libarcus-qtcura
		>=dev-libs/protobuf-3
	)
	dev-libs/polyclipping
	dev-libs/rapidjson
	dev-libs/stb
	"
DEPEND="${RDEPEND}
	dev-util/pkgconf
	test? ( dev-cpp/gtest )"
BDEPEND="doc? ( app-doc/doxygen )"

src_configure() {
	local mycmakeargs=( 
		"-DBUILD_TESTS=$(usex test ON OFF)" 
		"-DENABLE_ARCUS=$(usex arcus ON OFF)"
		"-DENABLE_OPENMP=$(usex openmp ON OFF)"
	)
	cmake_src_configure
}

src_compile() {
	if use doc; then
		doxygen
		mv docs/html . || die
		find html -name '*.md5' -or -name '*.map' -delete || die
		DOCS+=( html )
	fi
}
