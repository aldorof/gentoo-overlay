EAPI="7"

PYTHON_COMPAT=( python3_{4..8} )
inherit cmake-utils git-r3 fdo-mime gnome2-utils python-single-r1

DESCRIPTION="A 3D model slicing application for 3D printing"
HOMEPAGE="https://gitlab.com/qtcura/qtcura"
EGIT_REPO_URI="https://gitlab.com/qtcura/qtcura.git"
EGIT_BRANCH="master"
# EGIT_BRANCH="T6335"

LICENSE="AGPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+usb"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${PYTHON_DEPS}
	dev-libs/libarcus-qtcura:=[python,${PYTHON_SINGLE_USEDEP}]
	dev-libs/libsavitar-qtcura:=[python,${PYTHON_SINGLE_USEDEP}]
	dev-python/uranium-qtcura[${PYTHON_SINGLE_USEDEP}]
	$(python_gen_cond_dep '
		dev-python/scipy[${PYTHON_USEDEP}]
		usb? ( 
			dev-python/pyserial[${PYTHON_MULTI_USEDEP}] 
		)
		dev-python/zeroconf[${PYTHON_MULTI_USEDEP}]
	')
	media-gfx/qtcuraengine"
DEPEND="${RDEPEND}
	sys-devel/gettext"

src_prepare() {
	einfo "Patching location of version.json"
	sed -e "s:\"cura-lulzbot\",\"version.json\":\"share\", \"cura\",\"version.json\":" \
		-i cura/CuraApplication.py || die
	einfo "Patching cura_app.py for python version : ${EPYTHON}"
	sed -e "s:#!/usr/bin/env python3:#!/usr/bin/env ${EPYTHON}:" -i cura_app.py || die

	eapply_user
	cmake-utils_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DPYTHON_SITE_PACKAGES_DIR="$(python_get_sitedir)" )
	cmake-utils_src_configure
}

src_compile() {
	cmake-utils_src_compile
}

src_install() {

	cmake-utils_src_install
	doicon icons/*.png
	python_optimize "${D}${get_libdir}"

	einfo "Generating version.json"
	cp -R "${EROOT}"usr/share/cura/version.json .
	${PYTHON} "${FILESDIR}"/generate_version.py "${WORKDIR}"/"${P}" .
	insinto "${EROOT}"usr/share/cura
	doins version.json
}

pkg_preinst() {
	gnome2_icon_savelist
}

pkg_postinst() {
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
	gnome2_icon_cache_update
}

pkg_postrm() {
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
	gnome2_icon_cache_update
}
