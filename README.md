# gentoo-overlay

This is my personal overlay for gentoo packages. It's mostly 3D printing oriented

To add it to your gentoo system add this file to your repositories `/etc/portage/repos.conf/aldorof.conf`: 
```
[aldorof-overlay]
location = /usr/local/gentoo-overlays/aldorof
sync-type = git
sync-uri = https://gitlab.com/aldorof/gentoo-overlay
auto-sync = yes
```
and then sync it:
```shell
emaint sync -r aldorof-overlay
```
